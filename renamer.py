#Copyright 2018 Cameron Reid
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#imports
import os
from random import randint
import random

#The main code
def runScript():
	#Get the files
	files = [f for f in os.listdir('.') if os.path.isfile(f)]
	#Loop the files
	for f in files:
		#Skip if it's the renamer
		if f == "renamer.py":
			print("Skipped")
			continue
			
		#print(f)
		#Split the filename by colons. This code will add tilde to the name.
		fileNameSplit = f.split("~")
		
		#Checks if it's been split (So there's no out of reach vars)
		if len(fileNameSplit) >= 2:
			#There are at least 2 splits
			#Make a new var to clean code
			fileName = fileNameSplit[1]
			#Could be removed TBH
			print(fileName)
		else:
			#This file has no tilde. Use the full name
			fileName = fileNameSplit[0]
			print(fileName)
		
		#Make a random number, there will be dupes but the randomness is 
		#still there for the shuffle
		randNum = random.randrange(0, len(files))
		#randNum = random.randrange(0, len(files))
		#print(randNum)
		
		#Make the new name by combining the random number, a pipe, and the filename
		#No space after the pipe to keep infinite spaces from happening
		newName = str(randNum) + " ~" + fileName
		#Rename the file
		os.rename(f, newName)

#Welcome!
print("Welcome to this renamer!")
print("Are you sure you want to do this?")
print("This can mess things up badly if you're not careful")
a = input("Are you sure? y/n")
if a == "y":
	runScript()
else:
	print("Alright. Exiting.")


	
	
	
#Tell the user it's over :D
print("All done!")
